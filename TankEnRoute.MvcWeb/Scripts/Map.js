﻿var netherlands = { lat: 52.090737, lng: 5.121420 };
var map;
var infowindow;
var start;
var myLatlng;
var maxM = 50000;
var total = 50000;
var markers = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        mapTypeControl: false,
        center: netherlands,
        zoom: 8
    });

    new AutocompleteDirectionsHandler(map);
}

/**
 * @constructor 
*/
function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');

    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var originAutocomplete = new google.maps.places.Autocomplete(
        originInput, { placeIdOnly: true });
    var destinationAutocomplete = new google.maps.places.Autocomplete(
        destinationInput, { placeIdOnly: true });

    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
}

AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function (autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.place_id) {
            window.alert("Please select an option from the dropdown list.");
            return;
        }
        if (mode === 'ORIG') {
            me.originPlaceId = place.place_id;
        } else {
            me.destinationPlaceId = place.place_id;
        }

        deleteMarkers();

        me.route();
    });

};

AutocompleteDirectionsHandler.prototype.route = function () {
    if (!this.originPlaceId || !this.destinationPlaceId) {
        return;
    }
    var me = this;

    this.directionsService.route({
        origin: { 'placeId': this.originPlaceId },
        destination: { 'placeId': this.destinationPlaceId },
        travelMode: this.travelMode
    }, function (response, status) {
        if (status === 'OK') {
            me.directionsDisplay.setDirections(response);
          
            getChargePoints(response);

        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
};

function getChargePoints(response) {

    var start = response.routes[0].legs[0].start_address;
    var end = response.routes[0].legs[0].end_address;

    var data = JSON.stringify({
        "start": start,
        "end": end
    });

    $.ajax({
        type: "POST",
        url: "http://localhost:666/api/route/",
        data: data,
        success: success,
        contentType: "application/json",
        dataType: "json",
        cache:false
    });
}

function success( response)
{

    for (var i = 0; i < response.chargingPoints.length; i++) {
        var result = response.chargingPoints[i];

        var latitude = result.latitude;
        var longitude = result.longitude;
        var title = result.title;
        var town = result.town;
        var name = "Oplaadstation " + title + " " + town;

        createMarkerChargePoint(latitude, longitude, name);
    }
}


function createMarkerChargePoint(latitude, longitude, name) {
    myLatlng = new google.maps.LatLng(latitude, longitude);

    markers.push(marker);

    var image = '/Scripts/plug.png';
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
icon: image
    });
    google.maps.event.addListener(marker, "click", function () {
        if (infowindow) infowindow.close();
        infowindow = new google.maps.InfoWindow({
            content: name
        });
        infowindow.open(map, marker);
    });
    return marker;
}

function addInfoWindow(marker, name) {

    var infoWindow = new google.maps.InfoWindow({
        content: name
    });

    google.maps.event.addListener(marker, 'click', function () {

        for (var i = 0; i < markers.length; i++) {
            if (markers[i] != marker) {
                markers[i].infowindow.close();
            }
            
        }
        infoWindow.open(map, marker);
    });
}


// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

